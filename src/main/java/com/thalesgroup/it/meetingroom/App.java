/*
 *  * Copyright 2019 Thales Italia spa.  * 
 * 
 * This program is not yet licensed and this file may not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom;

public class App {

  public static void main(String[] args) throws Exception {
    final MainMenuManager mainMenuManager = new MainMenuManager();
    mainMenuManager.myMenu();

  }

}
