package controller;

import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.thalesgroup.it.meetingroom.datamanager.Jpa.ReservationDataInJpaManager;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.datamanager.exception.RoomAlreadyOccupiedException;
import com.thalesgroup.it.meetingroom.datamanager.exception.TwoDatesAreEquals;
import com.thalesgroup.it.meetingroom.datamanager.exception.WrongDateSelectionException;
import com.thalesgroup.it.meetingroom.model.Reservation;


@Path("/reservation")
public class ReservationServiceJpa {
	private  ReservationDataInJpaManager reservationDataManager = ReservationDataInJpaManager.getInstance();
	protected static final Logger LOGGER = Logger.getLogger(ReservationServiceJpa.class.getName());
	public ReservationServiceJpa() {
		
	}

	@POST
	@Path("/setData")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void setData(Reservation reservation) throws ItemNotFoundException, ItemAlreadyExistException, RoomAlreadyOccupiedException, WrongDateSelectionException, TwoDatesAreEquals {
		LOGGER.log(Level.INFO, "setData method in reservation servivce is called!");
		try {
			reservationDataManager.setReservation(reservation);
		} catch (ItemAlreadyExistException  e) {
			LOGGER.log(Level.SEVERE, "this reservation is already exists!",e);
			throw e;
		}
		catch (RoomAlreadyOccupiedException  e) {
			LOGGER.log(Level.SEVERE, "this room is not available at the specified time!",e);
			throw e;
		}
		catch (WrongDateSelectionException  e) {
			LOGGER.log(Level.SEVERE, "Wrong date selection!",e);
			throw e;
		}
		catch (TwoDatesAreEquals  e) {
			LOGGER.log(Level.SEVERE, "The Two dates are equal!",e);
			throw e;
		}
	}

	@GET
	@Path("/getData/{key}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Reservation getData(@PathParam("key") int key) throws ItemNotFoundException {
		try {
			return reservationDataManager.getData(key);
		} catch (ItemNotFoundException e) {
			LOGGER.log(Level.SEVERE, "this reservation is not exists!",e);
			throw e;
		}
	}

	@DELETE
	@Path("/deleteReservation/{key}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Reservation deleteReservation(@PathParam("key") Integer key) {
		return reservationDataManager.removeData(key);
	}
	
	
	@DELETE
	@Path("/deleteAllReservations/")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void deleteAllReservations() {
		 reservationDataManager.removeAllData();
	}


	@GET
	@Path("/getKeys")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Set<Integer> getKeys() {
		return reservationDataManager.getKeys();
	}
	
	
	
	@GET
	@Path("/getAllData")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Collection<Reservation> getAllData() throws ItemNotFoundException {
		try {
			return reservationDataManager.getAllData(null);
		} catch (ItemNotFoundException e) {
			LOGGER.log(Level.SEVERE, "this reservation is not exist!",e);
			throw e;
		}
		
	}
}
