package com.thalesgroup.it.meetingroom.datamanager.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.datamanagerJdbc.ReservationDataInJdbcManager;
import com.thalesgroup.it.meetingroom.model.Reservation;

public class ReservationDataInJdbcManagerTest {
	private Reservation reservation1;
	private Reservation reservation2;
	private Reservation reservation3;
	private Date startDate;
	private Date endDate;
	private Set<Integer> keysToCompare;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");;

	private DataManagerInterface<Reservation> reservationDataManager = ReservationDataInJdbcManager.getInstance();

	@Before
	public void initializeReservation() throws ParseException, ItemAlreadyExistException, ItemNotFoundException  {
		//startDate = dateFormat.parse("23/08/2015");
		//endDate = dateFormat.parse("24/08/2015");
		reservation1 = new Reservation(100, 333, 40, new Date(1576486800000L), new Date(1576490400000L));
		

		//startDate = dateFormat.parse("25/08/2015");
		//endDate = dateFormat.parse("26/08/2015");
		reservation2 = new Reservation(200, 441, 50, new Date(1576486800000L), new Date(1576490400000L));

		//startDate = dateFormat.parse("26/08/2015");
		//endDate = dateFormat.parse("27/08/2015");
		//reservation3 = new Reservation(300, 545, 60, startDate, endDate);
		reservationDataManager.setData(reservation1);
		reservationDataManager.setData(reservation2);
		//reservationDataManager.setData(reservation3);

		keysToCompare = new HashSet<>(Arrays.asList(100, 200, 300));
	}

	

	
	@Test // 2
	public void shouldContainReservation100() {
		Reservation reservation=null ;
		try {
			reservation=reservationDataManager.getData(100);
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		}
		assertTrue(reservation.getKey().equals(100));
	}

	@Test // 3
	public void shouldReturnAllkeys() {
		assertTrue(reservationDataManager.getKeys().equals(keysToCompare));
	}

	@Test // 4
	public void shouldBeAbleToDeleteReservation() {
		Reservation reservation = reservationDataManager.removeData(200);
		int actual = reservation.getKey();
		assertEquals(200, actual);
	}
	@After
	public void emptyDatabase() {
		Set<Integer> keys = new HashSet<>();
		keys = reservationDataManager.getKeys();
		for (Integer key : keys) {
			reservationDataManager.removeData(key);

		}
	}

}
