package com.thalesgroup.it.meetingroom.datamanagerJdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import com.thalesgroup.it.meetingroom.model.Room;

public class RoomDataInJdbcManager extends DataInJdbcManager<Room> {
	Set<Integer> keys = new HashSet<>();
	private static RoomDataInJdbcManager singleInstance = null;

	public static RoomDataInJdbcManager getInstance() {
		if (singleInstance == null) {
			singleInstance = new RoomDataInJdbcManager();
		}
		return singleInstance;
	}

	public RoomDataInJdbcManager() {
		super(Room.class);

	}

	@Override
	protected String getTableName() {
		return "room";
	}

	@Override
	protected Room mapResultSetToObject(ResultSet resultSet) throws SQLException {
		Integer id;
		String roomName;
		String roomSize;
		Room room = new Room();
		id = resultSet.getInt("id");
		roomName = resultSet.getString("ROOMNAME");
		roomSize = resultSet.getString("ROOMSIZE");
		room.setKey(id);
		room.setRoomName(roomName);
		room.setRoomSize(roomSize);
		return room;
	}

	@Override
	protected String mapObjectToQueryInsert(Room d) {
		Integer id = d.getKey();
		String roomName = d.getRoomName();
		String roomSize = d.getRoomSize();
		String query = "INSERT INTO room " + "VALUES (" + id + ",'" + roomName + "', '" + roomSize + "')";
		return query;

	}

	@Override
	public void updateData(Room d) {
		// TODO Auto-generated method stub
		
	}

}
