/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.model;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Person extends BaseData implements Data, Serializable {

  private static final long serialVersionUID = 1L;
  private String firstName;
  private String lastName;

  public Person() {

  }

  public Person(String firstName, String lastName, int key) {
    super(key);
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setFirstName(String fname) {
    firstName = fname;
  }

  public void setLastName(String lname) {
    lastName = lname;
  }
}
