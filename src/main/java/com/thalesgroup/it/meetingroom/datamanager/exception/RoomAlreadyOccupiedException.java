package com.thalesgroup.it.meetingroom.datamanager.exception;

public class RoomAlreadyOccupiedException extends Exception{
	private static final long serialVersionUID = 1L;
	public RoomAlreadyOccupiedException(String exceptionMessage) {
	    super(exceptionMessage);
	  }
}
