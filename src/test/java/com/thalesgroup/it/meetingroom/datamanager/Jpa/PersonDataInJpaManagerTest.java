package com.thalesgroup.it.meetingroom.datamanager.Jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.Jpa.PersonDataInJpaManager;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.Person;
import com.thalesgroup.it.meetingroom.model.Room;

public  class PersonDataInJpaManagerTest {

	private Person person1;
	private Person person2;
	private Person person3;
	private Person person4;
	Set<Integer> keysInDatabase;
	Set<Integer> keysToCompare;
	private DataManagerInterface<Person> personManager =  PersonDataInJpaManager.getInstance(); 

	@Before
	public void beforeDoingAll() throws ItemAlreadyExistException, ItemNotFoundException {

		person1 = new Person("faheem","jan",333);
		person2 = new Person("Sergio","ciampi", 441);
		person3 = new Person("Mama","papa",545);
		person4 = new Person("Luca","Genova",666);
		personManager.setData(person1);
		personManager.setData(person2);
		personManager.setData(person3);
		personManager.setData(person4);

		keysToCompare = new HashSet<>(Arrays.asList(333, 441, 545, 666));
	}

	@After
	public void emptyDatabase() {
		for (Integer key : personManager.getKeys()) {
			personManager.removeData(key);

		}
	}

	@Test // 1
	public void shouldBeAbleToRetrieveDataFromDatabase() {
		try {
			Person person = personManager.getData(333);
			assertEquals("faheem", person.getFirstName());
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Test // 2
	public void shouldReturnAllKeysFromDatabase() {
		assertEquals(keysToCompare, personManager.getKeys());
	}

	@Test // 3
	public void shouldContainPersonSergioInDatabase() {
		assertTrue(personManager.isDataInDatabase(441));
	}

	@Test // 4
	public void shouldBeAbleToDeleteDataFromDatabase() {
		Person person = personManager.removeData(545);
		assertEquals("Mama", person.getFirstName());
	}

}
