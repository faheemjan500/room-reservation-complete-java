/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.thalesgroup.it.meetingroom;

public class Utility {

  public final static String LINE_SEPARATOR = System.getProperty("line.separator");

  public static void printInConsole(String message) {
    System.out.println(message);
  }
}
