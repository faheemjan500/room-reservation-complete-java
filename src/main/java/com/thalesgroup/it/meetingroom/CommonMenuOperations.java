/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.thalesgroup.it.meetingroom;

import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class CommonMenuOperations {

  static MainMenuManager mainMenuManager = new MainMenuManager();

  @SuppressWarnings({ "deprecation", "null" })
  public static Calendar getDate(Scanner scanner) {
	  Calendar calendar = null;
    System.out.println("Enter year :");
    final int year = scanner.nextInt();
    
    System.out.println("Enter Month in numbers: :");
    final int month = scanner.nextInt();
    System.out.println("Enter day in number:");
    final int day = scanner.nextInt();
    System.out.println("Enter hour in number:");
    final int hour = scanner.nextInt();
    System.out.println("Enter Minutes:");
    final int minute = scanner.nextInt();
    calendar.set(year, month, day, hour, minute);
    return calendar;
  }

  static void goBackToMenu(Scanner scanner) throws Exception {
    System.out.println("To go back to the Menu page press 1 , or press any number to exit.");
    final int n = scanner.nextInt();
    if (n == 1) {
      mainMenuManager.myMenu();
    } else {
      System.out.println("Thank you very much for using our system. have a nice day!");
    }

  }
}
