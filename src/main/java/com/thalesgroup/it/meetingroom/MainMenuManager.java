/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.thalesgroup.it.meetingroom;

import static com.thalesgroup.it.meetingroom.Utility.*;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainMenuManager {

  private static final Logger LOGGER = Logger.getLogger(MainMenuManager.class.getName());
  private static final String menuMessage = "Click 1 to manage rooms" + LINE_SEPARATOR
      + "Click 2 to manage people" + LINE_SEPARATOR + "Click 3 to do reservation" + LINE_SEPARATOR
      + "******************************" + LINE_SEPARATOR + "Please choose from the Above menu:";

  public void myMenu() throws Exception {
    LOGGER.log(Level.INFO, "myMenu method is called by MainMenuManager");
    Scanner scanner = null;
    try {
      scanner = new Scanner(System.in);
      int select = 0;
      printInConsole(menuMessage);
      select = scanner.nextInt();
      switch (select) {
        case 1:
          RoomMenuManager.manageRooms(scanner);
          break;
        case 2:
          PersonMenuManager.managePeople(scanner);
          break;
        case 3:
          ReservationMenuManager.manageReservation(scanner);
          break;
        default:
          printInConsole("Sorry you clicked wrong button!");
      }
    } finally {
      scanner.close();
    }
  }
}
