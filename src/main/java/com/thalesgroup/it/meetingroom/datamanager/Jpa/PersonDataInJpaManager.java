package com.thalesgroup.it.meetingroom.datamanager.Jpa;

import com.thalesgroup.it.meetingroom.model.Person;

public class PersonDataInJpaManager extends DataInJpaManager<Person> {

	  private static PersonDataInJpaManager singleInstance = null;

	  public static PersonDataInJpaManager getInstance() {
	    if (singleInstance == null) {
	      singleInstance = new PersonDataInJpaManager();
	    }
	    return singleInstance;
	  }

	  private  PersonDataInJpaManager() {
		  super(Person.class);
	  }
	  
	  
	}

