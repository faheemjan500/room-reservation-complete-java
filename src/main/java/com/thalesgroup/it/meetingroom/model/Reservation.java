/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.model;

import java.io.Serializable;
import java.util.Date;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table()
public class Reservation extends BaseData implements Data,Serializable {

  private static final long serialVersionUID = 1L;
  private Integer personKey;
  private Integer roomKey;
  @Temporal(TemporalType.TIMESTAMP)
  //@JsonbDateFormat("dd/MM/yyyy")
  private Date startTime;
  
  @Temporal(TemporalType.TIMESTAMP)
  //@JsonbDateFormat("dd/MM/yyyy")
  private Date endTime;
  

  public Reservation() {

  }

  public Reservation(Integer key, Integer personKey, Integer roomKey, Date startTime,
		  Date endTime) {
    super(key);
    this.personKey = personKey;
    this.roomKey = roomKey;
    this.startTime = startTime;
    this.endTime = endTime;

  }


public Date getEndTime() {
    return endTime;
  }

  public Integer getPersonKey() {
    return personKey;
  }

  public Integer getRoomKey() {
    return roomKey;
  }

  public Date getStartTime() {
    return startTime;
  }

public void setPersonKey(Integer personKey2) {
	this.personKey=personKey2;
	
}

public void setRoomKey(Integer roomKey) {
	this.roomKey = roomKey;
}

public void setStartTime(Date startTime) {
	this.startTime = startTime;
}

public void setEndTime(Date endTime) {
	this.endTime = endTime;
}


}
