/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.datamanager.memory;

import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.Reservation;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReservationDataInMemoryManager extends DataInMemoryManager<Reservation> {

  private static final Logger LOGGER =
      Logger.getLogger(ReservationDataInMemoryManager.class.getName());
  private static ReservationDataInMemoryManager singleInstance = null;

  public static ReservationDataInMemoryManager getInstance() {
    if (singleInstance == null) {
      singleInstance = new ReservationDataInMemoryManager();
    }
    return singleInstance;
  }

  private ReservationDataInMemoryManager() {
  }

  public void setReservation(Reservation itemToStore) throws ItemAlreadyExistException {
    LOGGER.log(Level.INFO, "setReservation() method called");
    final Set<Integer> keys = this.getKeys();
    List<Reservation> reservations;
    try {

      reservations = getAllData(keys);
      for (final Reservation reservation : reservations) {
        if (reservation.getRoomKey().equals(itemToStore.getRoomKey())
            && reservation.getStartTime().equals(itemToStore.getStartTime())
            && reservation.getEndTime().equals(itemToStore.getEndTime())) {
          LOGGER.log(Level.WARNING, "item already exist in the system");
          throw new ItemAlreadyExistException("Item already exist");
        }
      }
    } catch (final ItemNotFoundException e) {
      LOGGER.log(Level.SEVERE, e.getMessage());
    }

    setData(itemToStore);

  }
}
