package com.thalesgroup.it.meetingroom.datamanagerJdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import com.thalesgroup.it.meetingroom.model.Reservation;

public class ReservationDataInJdbcManager extends DataInJdbcManager<Reservation> {

	private static ReservationDataInJdbcManager singleInstance = null;

	public static ReservationDataInJdbcManager getInstance() {
		if (singleInstance == null) {
			singleInstance = new ReservationDataInJdbcManager();
		}
		return singleInstance;
	}

	private ReservationDataInJdbcManager() {
		super(Reservation.class);
	}

	@Override
	protected Reservation mapResultSetToObject(ResultSet resultSet) throws SQLException {
		Integer id;
		Integer personKey;
		Integer roomKey;
		Date startTime;
		Date endTime;
		Reservation reservation = new Reservation();
		id = resultSet.getInt("id");
		personKey = resultSet.getInt("personKey");
		roomKey = resultSet.getInt("roomKey");
		startTime = resultSet.getDate("startTime");
		endTime = resultSet.getDate("endTime");
		reservation.setKey(id);
		reservation.setPersonKey(personKey);
		reservation.setRoomKey(roomKey);
		reservation.setStartTime(startTime);
		reservation.setEndTime(endTime);
		return reservation;
	}

	@Override
	protected String getTableName() {
		return "reservation";
	}

	@Override
	protected String mapObjectToQueryInsert(Reservation reservation) {
		Integer id = reservation.getKey();
		Integer personKey = reservation.getPersonKey();
		Integer roomKey = reservation.getRoomKey();
		Date startTime = reservation.getStartTime();
		Date endTime = reservation.getEndTime();
		
		String pattern = "yyyy-MM-dd hh:mm:ss";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		
		String query = "INSERT INTO reservation VALUES (" + id + ", " + personKey + "," + roomKey + ",'" + formatter.format(startTime) + "','" + formatter.format(endTime) + "' ) ";
		return query;
	}

	@Override
	public void updateData(Reservation d) {
		// TODO Auto-generated method stub
		
	}

}
