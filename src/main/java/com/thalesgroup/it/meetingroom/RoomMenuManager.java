/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.thalesgroup.it.meetingroom;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.memory.RoomDataInMemoryManager;
import com.thalesgroup.it.meetingroom.model.Room;

import static com.thalesgroup.it.meetingroom.Utility.LINE_SEPARATOR;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;




public class RoomMenuManager {
  //private static final Logger LOGGER = Logger.getLogger(PersonController.class.getName());
  static CommonMenuOperations commonMenuOperations = new CommonMenuOperations();
  private final static DataManagerInterface<Room> roomDataManager =
      RoomDataInMemoryManager.getInstance();
  private static List<Room> allRoomsInDatabase = new ArrayList<>();

  public static void manageRooms(Scanner scanner) throws Exception {
    final String roomsMenu =
        "you entered 1 to manage rooms" + LINE_SEPARATOR + "To add a room please click 1"
            + LINE_SEPARATOR + "To delete a room please click 2" + LINE_SEPARATOR
            + "To see a room please click 3" + LINE_SEPARATOR + "To see all the rooms click 4";
    final String roomMenuInsertRoomName = "you entered 1" + LINE_SEPARATOR + "Enter Room name:";
    Utility.printInConsole(roomsMenu);
    final int roomOperation = scanner.nextInt();
    switch (roomOperation) {
      case 1:
        Utility.printInConsole(roomMenuInsertRoomName);
        final String roomName = scanner.next();
        System.out.println("Enter room size:");
        final String roomSize = scanner.next();
        System.out.println("Enter room key:");
        final Integer key = scanner.nextInt();
        final Room room = new Room(key, roomName, roomSize);
        roomDataManager.setData(room);
        System.out.println("Room has been added to our system. thank you very much!");
        CommonMenuOperations.goBackToMenu(scanner);
        break;
      case 2:
        System.out.println("you entered 2");
        System.out.println("please enter the room number to delete:");
        final Integer roomNumber = scanner.nextInt();
        roomDataManager.removeData(roomNumber);
        System.out.println("the room you specified deleted successfully!");
        CommonMenuOperations.goBackToMenu(scanner);
        break;
      case 3:
        System.out.println("you entered 3");
        System.out.println("Please enter the room number to see the room:");
        final Integer roomNumber1 = scanner.nextInt();
        System.out.println("The room with key : " + roomNumber1 + " is : "
            + roomDataManager.getData(roomNumber1).getRoomName() + " and its size is :"
            + roomDataManager.getData(roomNumber1).getRoomSize());
        CommonMenuOperations.goBackToMenu(scanner);
        break;
      case 4:
        final Set<Integer> keys = roomDataManager.getKeys();
        allRoomsInDatabase = roomDataManager.getAllData(keys);
        System.out.println("All the rooms are the following :\n");
        for (final Room roomOut : allRoomsInDatabase) {
          Utility.printInConsole(roomOut.toString() + LINE_SEPARATOR);
        }
        CommonMenuOperations.goBackToMenu(scanner);
        break;

      default:
        System.out.println("Sorry you didn't choose any operation from the Rooms Menu!");
    }
  }

}
