package com.thalesgroup.it.meetingroom.datamanager.jdbc;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


import java.util.HashSet;
import java.util.Set;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.datamanagerJdbc.PersonDataInJdbcManager;
import com.thalesgroup.it.meetingroom.model.Person;

public class PersonDataInJdbcManagerTest {
	private DataManagerInterface<Person> personDataManager = PersonDataInJdbcManager.getInstance();
	private Person person1;
	private Person person2;
	private Person person3;

	@Before
	public void beforeDoingAll() {
		person1 = new Person("Faheem", "jan", 60);
		person2 = new Person("Sergio", "ciampi", 70);
		person3 = new Person("Luca", "geneva", 80);
		try {
			personDataManager.setData(person1);
			personDataManager.setData(person2);
			personDataManager.setData(person3);
		} catch (ItemAlreadyExistException e) {
			e.printStackTrace();
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Test // 1
	public void getSpecificPersonFromDatabase() {
		Person person = null;

		try {
			person = personDataManager.getData(60);
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		}
		assertEquals("Faheem", person.getFirstName());

	}

	@Test // 3
	public void souldReturnAllKeys() {
		Set<Integer> keys = new HashSet<>();
		keys = personDataManager.getKeys();
		assertTrue(keys.contains(70));
	}

	@Test // 4
	public void shouldBeAbleToRemovePerson() {
		personDataManager.removeData(80);
		assertFalse(personDataManager.getKeys().contains(80));
	}

	@After
	public void cleanUpDatabase() {
		Set<Integer> keys = new HashSet<>();
		keys = personDataManager.getKeys();
		for (Integer key : keys) {
			personDataManager.removeData(key);
		}
	}

	 @Test //2
		public void shouldBeAbleToSetData() throws ItemAlreadyExistException, ItemNotFoundException {
			Person person = new Person("bica","jaka",777);
			personDataManager.setData(person);
		}
}
