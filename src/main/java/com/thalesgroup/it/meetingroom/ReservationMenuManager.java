/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.thalesgroup.it.meetingroom;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.datamanager.memory.ReservationDataInMemoryManager;
import com.thalesgroup.it.meetingroom.model.Reservation;

import static com.thalesgroup.it.meetingroom.Utility.LINE_SEPARATOR;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ReservationMenuManager {

  private final static DataManagerInterface<Reservation> reservationDataManager =
      ReservationDataInMemoryManager.getInstance();

  static CommonMenuOperations commonMenuOperations = new CommonMenuOperations();

  public static void deletingReservation(Scanner scanner) {
    System.out.println("Enter reservation key :");
    final Integer reservationKey = scanner.nextInt();
    final Reservation reservation = reservationDataManager.removeData(reservationKey);
    System.out.println("The reservation for room with key:" + reservation.getRoomKey()
        + "for person with key :" + reservation.getPersonKey() + "is removed\n");
  }

  public static void displayAllReservations() throws ItemNotFoundException {
    List<Reservation> allReservationInDatabase = new ArrayList<>();
    final Set<Integer> keys = reservationDataManager.getKeys();
    allReservationInDatabase = reservationDataManager.getAllData(keys);
    System.out.println("We have the following reservations in our system....");
    for (final Reservation reservation : allReservationInDatabase) {
      System.out.println("Reservation by the person with key : " + reservation.getPersonKey() + "\n"
          + " for Room number : " + reservation.getRoomKey() + " \n");

    }
  }

  public static void manageReservation(Scanner scanner) throws Exception {

    System.out.println("you entered 3");
    final String message = "Click 1 to insert reservation:" + LINE_SEPARATOR
        + "Click 2 to delete reservation" + LINE_SEPARATOR + "Click 3 to see all reservations";
    Utility.printInConsole(message);
    final int choose = scanner.nextInt();

    switch (choose) {
      case 1:
        reservingRoom(scanner);
        CommonMenuOperations.goBackToMenu(scanner);
        break;
      case 2:
        deletingReservation(scanner);
        CommonMenuOperations.goBackToMenu(scanner);
        break;
      case 3:
        displayAllReservations();
        CommonMenuOperations.goBackToMenu(scanner);
        break;
    }

  }

  private static void reservingRoom(Scanner scanner) throws Exception {
    System.out.println("Enter person's key :");
    final Integer personKey = scanner.nextInt();
    System.out.println("Enter reservation key :");
    final Integer reservatioKey = scanner.nextInt();
    System.out.println("Enter the room key you want to reserve :");
    final Integer roomKey = scanner.nextInt();
    System.out.println("Enter start date for which you want to do the reservation:");
    final Calendar startTime = CommonMenuOperations.getDate(scanner);
    System.out.println("Now Enter the ending date for your reservation:");
    final Calendar endTime = CommonMenuOperations.getDate(scanner);
    //final Reservation reservation =
        //new Reservation(reservatioKey, personKey, roomKey, startTime, endTime);
   // reservationDataManager.setData(reservation);
    // reservationDataManager.setReservation(reservation);

   // CommonMenuOperations.goBackToMenu(scanner);
  }

}
