package com.thalesgroup.it.meetingroom.datamanager.Jpa;

import com.thalesgroup.it.meetingroom.model.Room;

public class RoomDataInJpaManager extends DataInJpaManager<Room> {

	private static RoomDataInJpaManager singleInstance = null;

	public static RoomDataInJpaManager getInstance() {
		if (singleInstance == null) {
			singleInstance = new RoomDataInJpaManager();
		}
		return singleInstance;
	}

	private RoomDataInJpaManager() {
		super(Room.class);
	}
}
