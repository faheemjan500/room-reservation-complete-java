package com.thalesgroup.it.meetingroom.datamanagerJdbc;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

import com.thalesgroup.it.meetingroom.model.Person;

public class PersonDataInJdbcManager extends DataInJdbcManager<Person> {
	Set<Integer> keys = new HashSet<>();
	private static PersonDataInJdbcManager singleInstance = null;

	public static PersonDataInJdbcManager getInstance() {
		if (singleInstance == null) {
			singleInstance = new PersonDataInJdbcManager();
		}
		return singleInstance;
	}

	private PersonDataInJdbcManager() {
		super(Person.class);

	}

	protected String getTableName() {
		return "person";
	}

	protected Person mapResultSetToObject(ResultSet resultSet) throws SQLException {
		Integer id;
		String firstName;
		String lastName;
		Person person = new Person();
		id = resultSet.getInt("id");
		firstName = resultSet.getString("firstName");
		lastName = resultSet.getString("lastName");
		person.setKey(id);
		person.setFirstName(firstName);
		person.setLastName(lastName);
		return person;
	}

	protected String mapObjectToQueryInsert(Person person) {
		Integer id = person.getKey();
		String firstName = person.getFirstName();
		String lastName = person.getLastName();
		String query = "INSERT INTO person " + "VALUES (" + id + ",'" + firstName + "', '" + lastName + "')";
		return query;
	}

	@Override
	public void updateData(Person d) {
		// TODO Auto-generated method stub
		
	}

}