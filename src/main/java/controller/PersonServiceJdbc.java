package controller;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.thalesgroup.it.meetingroom.datamanagerJdbc.DataInJdbcManager;
import com.thalesgroup.it.meetingroom.datamanagerJdbc.PersonDataInJdbcManager;
import com.thalesgroup.it.meetingroom.model.Person;

@Path("/personJdbc")
public class PersonServiceJdbc {
private DataInJdbcManager<Person> personDataManager = PersonDataInJdbcManager.getInstance();

public PersonServiceJdbc() {
	super();
	
}
@GET
@Path("/getAllPersonData")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public void getAllPersons() {

}
}
