/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.model;

import javax.persistence.Entity;

@Entity
public class Room extends BaseData implements Data {

private static final long serialVersionUID = 1L;
private String roomName;
 private String roomSize;

  public Room() {

  }

  public Room(int key, String name, String size) {
    super.setKey(key);
    this.roomName = name;
    this.roomSize = size;
  }

  public String getRoomName() {
    return roomName;
  }

  public String getRoomSize() {
    return roomSize;
  }

  public void setRoomName(String roomName) {
    this.roomName = roomName;
  }

  public void setRoomSize(String roomSize) {
    this.roomSize = roomSize;
  }

  @Override
  public String toString() {
    return "Room [roomName=" + roomName + ", roomSize=" + roomSize + ", Key " + getKey() + "]";
  }

}
