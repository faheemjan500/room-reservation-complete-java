/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.thalesgroup.it.meetingroom;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.memory.PersonDataInMemoryManager;
import com.thalesgroup.it.meetingroom.model.Person;

import static com.thalesgroup.it.meetingroom.Utility.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class PersonMenuManager {
  private final static DataManagerInterface<Person> personDataManager =
      PersonDataInMemoryManager.getInstance();
  private static List<Person> allPeopleInDatabase = new ArrayList<>();

  public static void managePeople(Scanner scanner) throws Exception {
    final String peopleMenu = "you entered 2 to manage people" + LINE_SEPARATOR
        + "Click 1 to add person:" + LINE_SEPARATOR + "Click 2 to delete person" + LINE_SEPARATOR
        + "Click 3 to see details of a person " + LINE_SEPARATOR
        + "Clik 4 to see all the people in our database";
    printInConsole(peopleMenu);

    final int personOperation = scanner.nextInt();
    switch (personOperation) {
      case 1:
        printInConsole("Enter person first name :");
        final String firstName = scanner.next();
        printInConsole("Enter person last name :");
        final String lastName = scanner.next();
        printInConsole("Enter key for the person :");
        final int key = scanner.nextInt();
        final Person person = new Person(firstName, lastName, key);
        personDataManager.setData(person);
        printInConsole("Person added ,thanks!");
        CommonMenuOperations.goBackToMenu(scanner);
        break;
      case 2:
        printInConsole("you entered 2");
        printInConsole("Please enter the key of the person to delete:");
        final int personKey = scanner.nextInt();
        personDataManager.removeData(personKey);
        printInConsole("Person deleted successfully");
        CommonMenuOperations.goBackToMenu(scanner);
        break;
      case 3:
        printInConsole("you entered 3");
        printInConsole("Please enter the person key to see his details :");
        final Integer keyToPerson = scanner.nextInt();
        printInConsole("The person with key :" + keyToPerson + " is :"
            + personDataManager.getData(keyToPerson).getFirstName() + " "
            + personDataManager.getData(keyToPerson).getLastName());
        CommonMenuOperations.goBackToMenu(scanner);
        break;
      case 4:
        final Set<Integer> keys = personDataManager.getKeys();
        allPeopleInDatabase = personDataManager.getAllData(keys);
        printInConsole("All the people in our database are the following :\n");
        for (final Person personOut : allPeopleInDatabase) {
          printInConsole(personOut.getFirstName() + " " + personOut.getLastName() + " \n");
        }
        CommonMenuOperations.goBackToMenu(scanner);
        break;
    }

  }
}
