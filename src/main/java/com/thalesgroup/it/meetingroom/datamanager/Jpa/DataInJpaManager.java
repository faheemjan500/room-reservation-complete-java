package com.thalesgroup.it.meetingroom.datamanager.Jpa;
/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.BaseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DataInJpaManager<D extends BaseData> implements DataManagerInterface<D> {

	public DataInJpaManager(Class<D> clazz) {
		super();
		this.clazz = clazz;

	}

	private Class<D> clazz;
	private final EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("meeting-room-reservation-app");
	private final EntityManager entityManager = entityManagerFactory.createEntityManager();
	protected static final Logger LOGGER = Logger.getLogger(DataInJpaManager.class.getName());

	@Override
	public D getData(Integer key) throws ItemNotFoundException,NullPointerException {
		D d = null;
		LOGGER.log(Level.INFO, "getData in database is called");
		try {
			d = entityManager.find(clazz, key);
		} catch (NullPointerException e) {
			LOGGER.log(Level.SEVERE, "data is not exist!",e);
			throw e;
		}

		return d;
	}

	@Override
	public D removeData(Integer key) {
		D d = null;
		LOGGER.log(Level.INFO, "removeData method in database is called.");
		entityManager.getTransaction().begin();
		d = entityManager.find(clazz, key);
		entityManager.remove(d);
		entityManager.getTransaction().commit();
		return d;
	}

	@Override
	public void setData(D itemToStore) throws ItemAlreadyExistException {
		LOGGER.log(Level.INFO, "setData method in database is called.");
		entityManager.getTransaction().begin();
		entityManager.persist(itemToStore);
		entityManager.getTransaction().commit();
	}

	@Override
	public void setDatas(List<D> datas) {
		for (D d : datas) {
			try {
				this.setData(d);
			} catch (ItemAlreadyExistException e) {
				e.printStackTrace();
			}
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<D> getAllData(Set<Integer> keys) throws ItemNotFoundException {
		LOGGER.log(Level.INFO, "getAllData method in database is called.");
		List<D> allItems = new ArrayList<>();
		entityManager.getTransaction().begin();
		allItems = entityManager.createQuery("SELECT d FROM "+ clazz.getName()+  " d").getResultList();
		entityManager.getTransaction().commit();
		return allItems;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  Set<Integer> getKeys() {
		LOGGER.log(Level.INFO, "getKeys method in database is called.");
		List<D> allItems = new ArrayList<>();
		entityManager.getTransaction().begin();
		allItems = entityManager.createQuery("SELECT d FROM " +clazz.getName()+  " d" ).getResultList();
		entityManager.getTransaction().commit();
		Set<Integer> set = new HashSet<>();
		for (D d : allItems) {
			set.add(d.getKey());
		}
		return set;
	}

	public boolean isDataInDatabase(Integer key) {
		LOGGER.log(Level.INFO, "isDataInDatabae method is called");
		if (entityManager.find(clazz, key) != null)
			return true;
		else
			return false;
	}

	@Override
	public void updateData(D d) {
		LOGGER.log(Level.INFO, "updateData method in database is called");
		entityManager.getTransaction().begin();
		entityManager.merge(d);
		entityManager.getTransaction().commit();
	}
	public void removeAllData() {
		for (Integer key : this.getKeys()) {
			this.removeData(key);

		}
	}


}
