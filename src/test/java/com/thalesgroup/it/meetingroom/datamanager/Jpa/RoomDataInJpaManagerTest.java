package com.thalesgroup.it.meetingroom.datamanager.Jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.Room;

public class RoomDataInJpaManagerTest {

	private DataManagerInterface<Room> roomDataManager = RoomDataInJpaManager.getInstance();
	private Room room1;
	private Room room2;
	private Room room3;

	Set<Integer> keysFromDatabase;
	Set<Integer> keysToCompare;

	@Before
	public void beforeDoingAll() throws ItemAlreadyExistException, ItemNotFoundException {

		room1 = new Room(40, "CoffeRoom", "Small");
		room2 = new Room(50, "MeetingRoom", "Medium");
		room3 = new Room(60, "RestRoom", "Small");
		roomDataManager.setData(room1);
		roomDataManager.setData(room2);
		roomDataManager.setData(room3);
		keysFromDatabase = new HashSet<>();
		keysToCompare = new HashSet<>(Arrays.asList(40, 50, 60));
	}

	@After
	public void emptyDatabase() {
		for (Integer key : roomDataManager.getKeys()) {
			roomDataManager.removeData(key);

		}
	}

	@Test // 2
	public void shouldDeleteDataFromDatabase() {
		Room room = roomDataManager.removeData(60);
		assertEquals("RestRoom", room.getRoomName());
	}

	@Test // 3
	public void shouldRetrieveDataFromDatabase() {
		try {
			Room room = roomDataManager.getData(50);
			assertEquals("MeetingRoom", room.getRoomName());
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Test // 4
	public void shouldReturnAllKeysFromDatabase() {
		keysFromDatabase = roomDataManager.getKeys();
		assertTrue(keysFromDatabase.equals(keysToCompare));
	}

	@Test // 5
	public void shouldContainRoomCoffeRoomInDatabase() {
		assertTrue(roomDataManager.isDataInDatabase(40));
	}

}
