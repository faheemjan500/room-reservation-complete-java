package com.thales.datamanager.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowMapper<D> {

	
	D map(ResultSet resultSet) throws SQLException;
}
