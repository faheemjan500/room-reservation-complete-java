package controller;

import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.Jpa.PersonDataInJpaManager;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.Person;

@Path("/personJpa")
public class PersonServiceJpa {
	private DataManagerInterface<Person> personDataManager = PersonDataInJpaManager.getInstance();
	protected static final Logger LOGGER = Logger.getLogger(PersonServiceJpa.class.getName());
	public PersonServiceJpa() {

	}

	@POST
	@Path("/setData")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void setData(Person person) throws Exception {
		try {
			personDataManager.setData(person);
		} catch (ItemAlreadyExistException | ItemNotFoundException e) {
			LOGGER.log(Level.SEVERE, "this person is already exist,cannot be added!",e);
			throw e;
		}
	}
	@PUT
	@Path("/updateData")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void updateData(Person person)  {
		personDataManager.updateData(person);
	}
    
	@GET
	@Path("/getData/{key}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Person getData(@PathParam("key") int key) throws ItemNotFoundException {

		return personDataManager.getData(key);

	}

	@DELETE
	@Path("/deletePerson/{key}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Person deletePerson(@PathParam("key") Integer key) {
		return personDataManager.removeData(key);
	}

	@GET
	@Path("/getPersonKeys")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Set<Integer> getKeys() {
		return personDataManager.getKeys();
	}

	@GET
	@Path("/getAllPersonData")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Collection<Person> getAllData() throws ItemNotFoundException {

		return personDataManager.getAllData(null);

	}
}
