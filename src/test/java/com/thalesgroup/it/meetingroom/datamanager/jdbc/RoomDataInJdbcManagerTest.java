package com.thalesgroup.it.meetingroom.datamanager.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.datamanagerJdbc.RoomDataInJdbcManager;
import com.thalesgroup.it.meetingroom.model.Room;

public class RoomDataInJdbcManagerTest {
	private DataManagerInterface<Room> roomDataManager = RoomDataInJdbcManager.getInstance();
	private Connection conn = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	private Room room1;
	private Room room2;
	private Room room3;
	private Room room4;

	@Before
	public void beforeDoingAll() {
		room1 = new Room(101, "SalaMeeting", "small");
		room2 = new Room(102, "SalaRelax", "medium");
		room3 = new Room(103, "SalaTV", "big");
		room4 = new Room(104, "SalaStudio", "medium");
		try {
			roomDataManager.setData(room1);
			roomDataManager.setData(room2);
			roomDataManager.setData(room3);
			roomDataManager.setData(room4);
		} catch (ItemAlreadyExistException e) {
			e.printStackTrace();
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Test // 1
	public void getSpecificRoomFromDatabase() {
		Room room = null;

		try {
			room = roomDataManager.getData(101);
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		}
		assertEquals("SalaMeeting", room.getRoomName());
	}

	@Test // 2
	public void souldReturnAllKeys() {
		Set<Integer> keys = new HashSet<>();
		keys = roomDataManager.getKeys();
		assertTrue(keys.contains(101));
	}

	@Test // 3
	public void shouldBeAbleToRemovePerson() {
		roomDataManager.removeData(103);
		assertFalse(roomDataManager.getKeys().contains(103));
	}

	@After
	public void cleanUpDatabase() {
		Set<Integer> keys = new HashSet<>();
		keys = roomDataManager.getKeys();
		for (Integer key : keys) {
			roomDataManager.removeData(key);
		}
	}
//	@Test // 2
//	public void shouldBeAbleToSetData() throws ItemAlreadyExistException, ItemNotFoundException {
//
//	}

}
