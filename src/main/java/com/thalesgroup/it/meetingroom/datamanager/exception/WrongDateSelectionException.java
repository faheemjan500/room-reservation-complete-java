package com.thalesgroup.it.meetingroom.datamanager.exception;

public class WrongDateSelectionException extends Exception {
	private static final long serialVersionUID = 1L;
	public WrongDateSelectionException(String exceptionMessage) {
	    super(exceptionMessage);
	  }
}
