/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.datamanager.memory;

import com.thalesgroup.it.meetingroom.model.Person;

public class PersonDataInMemoryManager extends DataInMemoryManager<Person> {

  private static PersonDataInMemoryManager singleInstance = null;

  public static PersonDataInMemoryManager getInstance() {
    if (singleInstance == null) {
      singleInstance = new PersonDataInMemoryManager();
    }
    return singleInstance;
  }

  private PersonDataInMemoryManager() {
  }
}
