package com.thalesgroup.it.meetingroom.datamanager.exception;

public class TwoDatesAreEquals extends Exception {

	private static final long serialVersionUID = 1L;
	public TwoDatesAreEquals(String exceptionMessage) {
	    super(exceptionMessage);
	  }

}
