package com.thalesgroup.it.meetingroom.datamanager.Jpa;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.cj.exceptions.WrongArgumentException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.datamanager.exception.RoomAlreadyOccupiedException;
import com.thalesgroup.it.meetingroom.datamanager.exception.TwoDatesAreEquals;
import com.thalesgroup.it.meetingroom.datamanager.exception.WrongDateSelectionException;
import com.thalesgroup.it.meetingroom.model.Reservation;

public class ReservationDataInJpaManager extends DataInJpaManager<Reservation> {

	protected static final Logger LOGGER = Logger.getLogger(ReservationDataInJpaManager.class.getName());
    List<Reservation> allReservations = new ArrayList();
  private static ReservationDataInJpaManager singleInstance = null;

  public static ReservationDataInJpaManager getInstance() {
    if (singleInstance == null) {
      singleInstance = new ReservationDataInJpaManager();
    }
    return singleInstance;
  }

  private ReservationDataInJpaManager() {
	  super(Reservation.class);
  }

  @SuppressWarnings("deprecation")
public void setReservation(Reservation itemToStore) throws ItemAlreadyExistException, ItemNotFoundException, RoomAlreadyOccupiedException ,WrongDateSelectionException, TwoDatesAreEquals{
    LOGGER.log(Level.INFO, "setReservation() method in database is called");
    
//    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm");
//    simpleDateFormat.format(itemToStore.getStartTime());
//    simpleDateFormat.format(itemToStore.getEndTime());
    
    Instant startInstant = itemToStore.getStartTime().toInstant();
    Instant endInstant = itemToStore.getEndTime().toInstant();
    
    itemToStore.setStartTime(Date.from(startInstant.truncatedTo(ChronoUnit.MINUTES)));
    itemToStore.setEndTime(Date.from(endInstant.truncatedTo(ChronoUnit.MINUTES)));

    
  if(itemToStore.getEndTime().equals(itemToStore.getStartTime())) {
	 throw new TwoDatesAreEquals("Dates cannot be equal!");
}
    
    if(itemToStore.getEndTime().compareTo(itemToStore.getStartTime())<0) {
   	 throw new WrongDateSelectionException("Start time should be earlier than end time!");
    }
    
     Set<Integer> keys = new HashSet<>();
     keys = this.getKeys();
     allReservations = this.getAllData(keys);
      for(Reservation reservation : allReservations) {
    	    if( reservation.getRoomKey().equals(itemToStore.getRoomKey())) {
    	    	 if(    
    	    			 ( itemToStore.getStartTime().getTime() >= reservation.getStartTime().getTime() && itemToStore.getEndTime().getTime() <= reservation.getEndTime().getTime() ) || 
    	    			 ( itemToStore.getStartTime().getTime()<=reservation.getStartTime().getTime() && itemToStore.getEndTime().getTime()>reservation.getStartTime().getTime() ) ||
    	    			 (itemToStore.getStartTime().getTime() < reservation.getEndTime().getTime() && itemToStore.getEndTime().getTime()>=reservation.getEndTime().getTime() ) 
    	    			 )
    	    	   {
    	    		throw new RoomAlreadyOccupiedException("room is already occupied at this time!");
    	    	   }
    	    }
      }
      this.setData(itemToStore);
  }
}
