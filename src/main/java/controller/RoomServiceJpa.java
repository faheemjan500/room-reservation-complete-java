package controller;

import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.thalesgroup.it.meetingroom.datamanager.Jpa.RoomDataInJpaManager;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.Person;
import com.thalesgroup.it.meetingroom.model.Room;

@Path("/room")
public class RoomServiceJpa {
	private RoomDataInJpaManager roomDataManager = RoomDataInJpaManager.getInstance();

	protected static final Logger LOGGER = Logger.getLogger(RoomServiceJpa.class.getName());
	public RoomServiceJpa() {

	}

	@POST
	@Path("/setData")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void setData(Room room) throws ItemAlreadyExistException {
		try {
			roomDataManager.setData(room);
		} catch (ItemAlreadyExistException e) {
			LOGGER.log(Level.INFO, "ItemAlreadyExistException is occured!");
			throw e;
		}
	}
	@PUT
	@Path("/updateData")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void updateData(Room room)  {
		roomDataManager.updateData(room);
	}

	@GET
	@Path("/getData/{key}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Room getData(@PathParam("key") int key) throws ItemNotFoundException {
		try {
			return roomDataManager.getData(key);
		} catch (ItemNotFoundException e) {
			LOGGER.log(Level.SEVERE, "room is not exist!",e);
			throw e;
		}
		
	}

	@DELETE
	@Path("/deleteRoom/{key}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Room deletePerson(@PathParam("key") Integer key) {
		return roomDataManager.removeData(key);
	}

	@GET
	@Path("/getRoomKeys")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Set<Integer> getKeys() {
		return roomDataManager.getKeys();
	}
	@GET
	@Path("/getAllRoomData")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Collection<Room> getAllData() throws ItemNotFoundException {
		try {
			return roomDataManager.getAllData(null);
		} catch (ItemNotFoundException e) {
			LOGGER.log(Level.SEVERE, "room is not exist!", e);
			throw e;
		}
		
	}
   

}
