package com.thalesgroup.it.meetingroom.datamanager.Jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.datamanager.exception.RoomAlreadyOccupiedException;
import com.thalesgroup.it.meetingroom.datamanager.exception.TwoDatesAreEquals;
import com.thalesgroup.it.meetingroom.datamanager.exception.WrongDateSelectionException;
import com.thalesgroup.it.meetingroom.model.Reservation;

public class ReservationDataInJpaManagerTest {

	private Reservation reservation1;
	private Reservation reservation2;
	private Reservation reservation3;
	private Date startDate;
	private Date endDate;
	private Set<Integer> keysToCompare;
	private ReservationDataInJpaManager reservationDataManager = ReservationDataInJpaManager.getInstance();
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");;

	@Before
	public void initializeReservation()
			throws ItemAlreadyExistException, ItemNotFoundException, ParseException, RoomAlreadyOccupiedException, WrongDateSelectionException, TwoDatesAreEquals {

		
		// 16/12/2019 9.00 16/12/2019 10.00
		reservation1 = new Reservation(100, 407, 40, new Date(1576486800000L), new Date(1576490400000L));
		reservation2 = new Reservation(200, 1235, 40, new Date(1576486800000L), new Date(1576490400000L));
		// 1576839600000-20/12/19 11.00 1576843200000-20/12/19 12.00
		reservation3 = new Reservation(300, 1236, 40, new Date(1576839600000L), new Date(1576843200000L));
		
		reservationDataManager.setReservation(reservation1);

		keysToCompare = new HashSet<>(Arrays.asList(100, 200, 300));
	}

	@After
	public void emptyDatabase() {
		for (Integer key : reservationDataManager.getKeys()) {
			reservationDataManager.removeData(key);

		}
	}

	@Test // 1
	public void shouldContainReservation100() {
		assertTrue(reservationDataManager.isDataInDatabase(100));
	}
	@Test // 2
	public void shouldBeAbleToMakeReservation300() throws ItemAlreadyExistException, ItemNotFoundException, RoomAlreadyOccupiedException, WrongDateSelectionException, TwoDatesAreEquals {
		reservationDataManager.setReservation(reservation3);
		assertTrue(reservationDataManager.isDataInDatabase(300));
	}
     //Test 3
	@Test(expected = RoomAlreadyOccupiedException.class) 
	public void shouldNotInsertReservation2() //this room is already reserved at this time!
			throws ItemAlreadyExistException, ItemNotFoundException, RoomAlreadyOccupiedException, WrongDateSelectionException, TwoDatesAreEquals {
		reservationDataManager.setReservation(reservation2);

//		@Test // 3
//		public void shouldReturnAllkeys() {
//			assertTrue(reservationDataManager.getKeys().equals(keysToCompare));
//		}
//	}
//	@Test // 4
//	public void shouldBeAbleToDeleteReservation() {
//		Reservation reservation = reservationDataManager.removeData(200);
//		int actual = reservation.getKey();
//		assertEquals(200, actual);
//	}

	}
}
