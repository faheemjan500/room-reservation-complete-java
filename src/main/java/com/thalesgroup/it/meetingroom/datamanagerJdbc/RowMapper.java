package com.thalesgroup.it.meetingroom.datamanagerJdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowMapper<D> {
	 D map(ResultSet resultSet) throws SQLException;
}
