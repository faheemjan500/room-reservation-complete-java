/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.datamanager.memory;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.BaseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataInMemoryManager<D extends BaseData> implements DataManagerInterface<D> {

  private static final Logger LOGGER = Logger.getLogger(DataInMemoryManager.class.getName());
  private final Map<Integer, D> data = new HashMap<>();

  @Override
  public D getData(Integer key) throws ItemNotFoundException {
    LOGGER.log(Level.INFO, "getting a data");
    if (data.containsKey(key)) {
      return data.get(key);
    } else {
      LOGGER.log(Level.FINEST, "item not found");
      return null;
    }
  }

  @Override
  public List<D> getAllData(Set<Integer> keys) throws ItemNotFoundException {
    LOGGER.log(Level.INFO, "getting all data.");
    final List<D> foundItems = new ArrayList<>();
    D foundItem = null;
    for (final Integer key : keys) {
      try {
        foundItem = getData(key);
        foundItems.add(foundItem);
      } catch (final ItemNotFoundException e) {
        LOGGER.log(Level.FINEST, e.getMessage());
      }
    }
    if (foundItems.isEmpty()) {
      System.out.println("Not found items with provided list of keys");
    }
    return foundItems;
  }

  @Override
  public Set<Integer> getKeys() {
    LOGGER.log(Level.INFO, "getKeys method is called.");
    return data.keySet();
  }

  @Override
  public D removeData(Integer key) {
    LOGGER.log(Level.INFO, "removeData method is called.");
    return data.remove(key);
  }

  @Override
  public void setData(D itemToStore) throws ItemAlreadyExistException {
    LOGGER.log(Level.INFO, "setData method is called.");
    data.put(itemToStore.getKey(), itemToStore);

  }

  @Override
  public void setDatas(List<D> itemsToStore) throws ItemAlreadyExistException {
    LOGGER.log(Level.INFO, "setDatas method is called.");
    for (final D itemToStore : itemsToStore) {
      setData(itemToStore);
    }
  }

@Override
public boolean isDataInDatabase(Integer key) {
	return this.getKeys().contains(key);
	
}

@Override
public void updateData(D d) {
	// TODO Auto-generated method stub
	
}
}
