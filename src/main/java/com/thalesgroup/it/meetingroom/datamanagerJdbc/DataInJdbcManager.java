package com.thalesgroup.it.meetingroom.datamanagerJdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.BaseData;
import com.thalesgroup.it.meetingroom.model.Person;

public abstract class DataInJdbcManager<D extends BaseData> implements DataManagerInterface<D> {
	protected Connection conn = null;
	private Class<D> clazz;

	public DataInJdbcManager(Class<D> clazz) {
		super();
		this.clazz = clazz;
		getConnection();
	}

	@Override
	public D getData(Integer key) throws ItemNotFoundException {
		String query = "SELECT * FROM " + getTableName() + " WHERE id =" + key;
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				return mapResultSetToObject(resultSet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public D removeData(Integer key) {
		String query = "DELETE FROM " + getTableName() + " WHERE id =" + key;
		Statement statement;
		try {
			statement = conn.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			return getData(key);
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected abstract D mapResultSetToObject(ResultSet resultSet) throws SQLException;

	protected abstract String getTableName();

	@Override
	public List<D> getAllData(Set<Integer> keys) throws ItemNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Integer> getKeys() {
		int id;
		String query = "SELECT id FROM " + getTableName();
		Set<Integer> keys = new HashSet<>();
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				id = resultSet.getInt("id");
				keys.add(id);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return keys;
	}

	@Override
	public boolean isDataInDatabase(Integer key) {

		try {
			if (this.getData(key) != null) {
				return true;
			}
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void setData(D d) {
		String query = mapObjectToQueryInsert(d);
		try {
			Statement statement = conn.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected abstract String mapObjectToQueryInsert(D d);

	@Override
	public void setDatas(List<D> datas) throws ItemAlreadyExistException, ItemNotFoundException {
		// TODO Auto-generated method stub

	}

	protected void getConnection() {
		try {
			String url = "jdbc:mysql://localhost:3306/jdbcdatabase";
			conn = DriverManager.getConnection(url, "root", "amaris2019");
		} catch (SQLException e) {
			throw new Error("Problem", e);
		} finally {
			try {
				if (conn == null) {
					conn.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}
}
